const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

let server;

/** async request */

const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if (error) reject(error)
        else resolve(response)
    });
});

describe("test rest api", () => {
    beforeEach("start server", async () => {
        server = app.listen(port);
    });

    describe("test functionality", () => {
        it("get / returns 200, which is ok", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/add',
                qs: { a: '1', b: '1' },
            };
            await arequest(options).then((res) => {
                console.log({ message: res.body })
                expect(res.body).to.equal('2');
            }).catch(() => {
                console.log({ res });
                expect(true).to.equal(false, 'add funtion failed');
            })
        });
    });

    // check if any test fails and quit the server if so
    afterEach(() => {
        server.close();
    });
});

